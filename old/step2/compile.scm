(define printf
  (lambda x
    (apply format #t x))) ;; from nanopass

(define (header)
  (format #t "section .text\n")
  (format #t "\tglobal scheme_entry\n")
  (format #t "scheme_entry:\n"))

(define (ret)
  (format #t "\tret\n"))

;; cf. https://practical-scheme.net/gauche/man/gauche-refj/Shu-Zhi-.html#g_t_6570_5024_306e_6f14_7b97
;; cf. runtime.c (runtime representation)
;; TODO: use binary literal. see gauche reference manual, or see nanopass.
(define (int-rep x)
  (logior (ash x 2) 0))

(define (char-rep x)
  (logior (ash (char->integer x) 8) 15))

(define (bool-rep x)
  (if x 159 31))

(define nil-rep 47)

;; for '() and ()
(define (mynull? obj)
  (equal? (list 'quote '()) obj))

(define (immediate? x)
  (or (integer? x) (char? x) (boolean? x) (mynull? x)))

;; scheme -> runtime representation
(define (immediate-rep x)
  (cond
   ((integer? x) (int-rep x))
   ((char? x) (char-rep x))
   ((boolean? x) (bool-rep x))
   ((mynull? x) nil-rep)
   (else
    (error "unsupported type."))))

(define (compile-integer x)
  (format #t "\tmov eax, ~s\n" x))

(define (operator expr) (car expr))

(define (primcall-operand1 expr) (cadr expr))

(define (primcall? expr)
  (and (= (length expr) 2)
       (symbol? (operator expr))))

(define (compile-primcall expr)
  (let ((op (operator expr)))
    (cond ((eq? op 'add1)
	   (compile-add1))
	  ((eq? op 'sub1)
	   (compile-sub1))
	  (else
	   (error "unknown primcall")))))

(define (compile-add1)
  (printf "\tinc eax\n"))

(define (compile-sub1)
  (printf "\tdec eax\n"))


(define (compile-expr expr)
  (cond ((immediate? expr)
	 (printf "\tmov eax, ~s\n" (immediate-rep expr)))
	((primcall? expr)
	 (compile-expr (primcall-operand1 expr))
	 (compile-primcall expr))
	(else
	 (error "unsupported expression."))))

(define (compile-main expr)
  (header)
  (compile-expr expr)
  (ret))

(define (build-program expr)
  (with-output-to-file "output.asm"
    (lambda () (compile-main expr)))
  (sys-system "nasm -f elf64 output.asm")
  (sys-system "gcc -c runtime.c")
  (sys-system "gcc output.o runtime.o"))
