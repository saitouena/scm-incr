(load "./utils.scm")

(define unary-primitives '(add1 sub1 integer->char char->integer zero? null? not integer? boolean? char? car cdr pair? make-vector vector? vector-length))

(define binary-primitives '(+ - * < = char=? cons vector-ref eq?))

(define arg3-primitives '(vector-set!))

(define vararg-primitives
  (list 'vector))

(define primitive-vars
  (list->set
   (append unary-primitives
	   binary-primitives;; binary
	   arg3-primitives
	   vararg-primitives)))

(define (immediate? x)
  (or (integer? x) (char? x) (boolean? x) (mynull? x)))

;; for '()
(define (mynull? obj)
  (equal? (list 'quote '()) obj))

(define (operator expr)
  (car expr))

(define operands cdr)

(define (primcall-operand1 expr) (cadr expr))

(define (unary-primcall? expr)
  (and (= (length expr) 2)
       (memq (operator expr) unary-primitives)))

(define (binary-primcall? expr)
  (and (= (length expr) 3)
       (memq (operator expr) binary-primitives)))

(define binary-operand1 cadr)
(define binary-operand2 caddr)

(define (arg3-primcall? expr)
  (and (= (length expr) 4)
       (memq (operator expr) arg3-primitives)))

(define operand1 cadr)
(define operand2 caddr)
(define operand3 cadddr)

(define variable? symbol?)

(define (let*? expr)
  (tagged-list? expr 'let*))

(define (bind-var bind)
  (car bind))

(define (bind-rhs bind)
  (cdr bind))

(define (let-bind-rhs bind)
  (cadr bind))

;; (let* ((x 3) (y x)) expr)
(define let-bindings cadr)
(define let-body cddr)

(define (if? expr)
  (tagged-list? expr 'if))

(define if-test cadr)
(define if-conseq caddr)
(define if-altern cadddr)

(define (begin? expr)
  (tagged-list? expr 'begin))

(define (begin-seq expr)
  (cdr expr))

(define (vararg-primcall? expr)
  (memq (car expr) vararg-primitives))

(define operands cdr)

(define (lambda? expr)
  (tagged-list? expr 'lambda))
(define lambda-args cadr)
(define lambda-body cddr)

(define t-lambda-args cadr)
(define t-lambda-fvars caddr)
(define t-lambda-body cdddr)

(define apply-op car)
(define apply-args cdr)

(define (quote? expr)
  (tagged-list? expr 'quote))

(define (text-of-quotation exp) (cadr exp)) ;; from SICP 4.1.2

(define (primcall? e)
  (or (unary-primcall? e) (binary-primcall? e) (arg3-primcall? e) (vararg-primcall? e)))

(define (set!? e)
  (tagged-list? e 'set!))

(define set!-var cadr)

(define set!-val caddr)
