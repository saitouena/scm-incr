(load "./syntax.scm")
(load "./utils.scm")

;; transform to core language
(define (transform expr)
  (annotate->labels-top (annotate-free-variable (lift-variable->vector (treat-quote (desugar expr))))))

(define (transform2 e)
  (lift-variable->vector (treat-quote (desugar e))))

(define (free-vars-seq body)
  (if (null? body)
      (make-set)
      (union (free-vars (car body)) (free-vars-seq (cdr body)))))

(define (free-vars expr)
  (cond ((immediate? expr) (make-set))
	((variable? expr) (make-set expr))
	((let*? expr) (free-vars-let* expr))
	((if? expr) (union (free-vars (if-test expr))
			   (union (free-vars (if-conseq expr))
				  (free-vars (if-altern expr)))))
	((begin? expr)
	 (free-vars-seq (begin-seq expr)))
	((lambda? expr)
	 (diff (free-vars-seq (lambda-body expr)) (list->set (lambda-args expr))))
	(else
	 (let ((free-vars-list (map free-vars expr)))
	   (fold-right union (make-set) free-vars-list)))))

(define (free-vars-let* expr)
  (let ((b* (let-bindings expr)) (body (let-body expr)))
    (let loop ((binds b*) (acc (make-set)) (defined (make-set)))
      (if (null? binds)
	  (union acc (diff (free-vars-seq body) defined))
	  (loop
	   (cdr binds)
	   (union acc
		  (diff (free-vars (rhs (car binds))) defined))
	   (insert defined (lhs (car binds))))))))

;; (lambda (x) ...) => (lambda (x) free-variables body)
(define (annotate-free-variable expr)
  (cond ((lambda? expr)
	 (cons 'lambda
	       (cons (lambda-args expr)
		     (cons (set->list (diff (free-vars expr) primitive-vars))
			   (annotate-free-variable (lambda-body expr))))))
	((list? expr)
	 (map annotate-free-variable expr))
	(else expr)))

;; (lambda (x) (y) body) => (labels ((f (code (x) (y) body))) (closure f y))
(define (annotate->labels-top expr)
  (receive (labels body) (annotate->labels '() expr)
	   (cons 'labels
		 (list labels body))))

(define (annotate->labels labels expr)
  (cond ((immediate? expr)
	 (values labels expr))
	((variable? expr)
	 (values labels expr))
	((if? expr)
	 (annotate->labels-if labels expr))
	((begin? expr)
	 (receive (l s) (annotate->labels-seq labels (begin-seq expr) '())
		  (values l (cons 'begin s))))
	((lambda? expr)
	 (annotate->labels-lambda labels expr))
	((let*? expr)
	 (annotate->labels-let labels expr))
	(else
	 (annotate->labels-apply labels expr))))

(define (annotate->labels-if labels expr)
  (let* ((test (if-test expr))
	 (conseq (if-conseq expr))
	 (altern (if-altern expr)))
    (receive (l1 test) (annotate->labels labels test)
	     (receive (l2 conseq) (annotate->labels l1 conseq)
		      (receive (l3 altern) (annotate->labels l2 altern)
			       (values l3 (list 'if test conseq altern)))))))

(define (annotate->labels-seq labels rest acc)
  (if (null? (cdr rest))
      (receive (l e) (annotate->labels labels (car rest))
	       (values l (reverse (cons e acc))))
      (receive (l e) (annotate->labels labels (car rest))
	       (annotate->labels-seq l (cdr rest) (cons e acc)))))

(define (annotate->labels-apply labels rest)
  (annotate->labels-seq labels rest '()))

(define (annotate->labels-lambda labels expr)
  (let ((args (t-lambda-args expr))
	(fvars (t-lambda-fvars expr))
	(body (t-lambda-body expr)))
    (receive (l e) (annotate->labels-seq labels body '())
	     (let ((c (cons 'code
			    (cons args
				  (cons fvars e))))
		   (f (unique-label-f)))
	       (values (append l (list (list f c)))
		       (cons 'closure
			     (cons f fvars)))))))

(define (annotate->labels-let labels expr)
  (let ((binds (let-bindings expr))
	(body (let-body expr)))
    (receive (l1 binds) (annotate->labels-binds labels binds '())
	     (receive (l2 body) (annotate->labels-seq l1 body '())
		      (values l2 (cons 'let* (cons binds body)))))))

(define (annotate->labels-binds labels binds acc)
  (if (null? binds)
      (values labels (reverse acc))
      (let* ((b (car binds))
	     (lhs (lhs b))
	     (rhs (rhs b))
	     (rest (cdr binds)))
	(receive (l rhs) (annotate->labels labels rhs)
		 (annotate->labels-binds l rest (cons (list lhs rhs) acc))))))

;; for quote
(define (retrieve-quote-form expr)
  (cond ((quote? expr) (make-set (text-of-quotation expr)))
	((immediate? expr) (make-set))
	((variable? expr) (make-set))
	((let*? expr) (retrieve-quote-form-let* expr))
	((if? expr) (union (retrieve-quote-form (if-test expr))
			   (union (retrieve-quote-form (if-conseq expr))
				  (retrieve-quote-form (if-altern expr)))))
	((begin? expr)
	 (retrieve-quote-form-seq (begin-seq expr)))
	((lambda? expr)
	 (retrieve-quote-form-seq (lambda-body expr)))
	(else ;; apply
	 (let ((quotes (map retrieve-quote-form expr)))
	   (fold-right union (make-set) quotes)))))

(define (retrieve-quote-form-seq seq)
  (fold-right union (make-set) (map retrieve-quote-form seq)))

(define (retrieve-quote-form-let* let-form)
  (let ((rhss (map rhs (let-bindings let-form))) (body (let-body let-form)))
    (union (fold-right union (make-set) (map retrieve-quote-form rhss))
	   (retrieve-quote-form-seq body))))

(define (make-constant-var-map constant-list)
  (let ((unique-vars (make-n-unique-vars (length constant-list))))
    (map cons constant-list unique-vars)))

(define (flip-constant-var-map constant-var-map) ;; ((constant . var) ..) => ((var . constant) ..)
  (map (lambda (e) (cons (cdr e) (car e))) constant-var-map))

(define (quote->var expr constant->var-map)
  (cond ((quote? expr)
	 (cdr (assoc (text-of-quotation expr) constant->var-map)))
	((list? expr)
	 (map (lambda (e) (quote->var e constant->var-map)) expr))
	(else expr)))

(define (treat-constant c)
  (cond ((null? c) ''()) ;; TODO: ok?
	((immediate? c) c)
	((pair? c) (treat-constant-pair c))
	((vector? c) (treat-constant-vector c))
	(else
	 (error "unsupported constant c=" c))))

(define (treat-constant-pair p)
  (if (null? p)
      '()
      (list 'cons (treat-constant (car p)) (treat-constant (cdr p)))))

(define (treat-constant-vector v)
  (cons 'vector (vector->list v)))

(define (make-let-binds var->constant-map)
  (if (null? var->constant-map)
      '()
      (let* ((b (car var->constant-map))
	     (lhs (car b))
	     (rhs (treat-constant (cdr b))))
	(cons (list lhs rhs) (make-let-binds (cdr var->constant-map))))))

(define (treat-quote expr)
  (let* ((constants (retrieve-quote-form expr))
	 (constant->var-map (make-constant-var-map (set->list constants)))
	 (var->constant-map (flip-constant-var-map constant->var-map))
	 (let-binds (make-let-binds var->constant-map)))
    (list 'let* let-binds (quote->var expr constant->var-map))))

(define (lift-variable->vector expr)
  (cond ((immediate? expr) expr)
	((variable? expr) (list 'vector-ref expr 0))
	((set!? expr) (lift-variable->vector-set! expr))
	((if? expr) (list 'if
			  (lift-variable->vector (if-test expr))
			  (lift-variable->vector (if-conseq expr))
			  (lift-variable->vector (if-altern expr))))
	((begin? expr)
	 (cons 'begin (map lift-variable->vector (begin-seq expr))))
	((lambda? expr)
	 (cons 'lambda
	       (cons (lambda-args expr)
		     (map lift-variable->vector (lambda-body expr)))))
	
	((let*? expr) (lift-variable->vector-let* expr))
	((primcall? expr) (lift-variable->vector-primcall expr))
	(else ;; user defined funcall
	 (lift-variable->vector-apply expr))))

(define (lift-variable->vector-let* expr)
  (let* ((b* (let-bindings expr))
	 (lhss (map lhs b*))
	 (rhss (map (lambda (b) (list 'vector (lift-variable->vector (rhs b)))) b*))) ;; wrap vector
    (cons 'let*
	  (cons (map (lambda (l r) (list l r)) lhss rhss)
		(map lift-variable->vector (let-body expr))))))

(define (lift-variable->vector-primcall expr)
  (cons (operator expr) (map lift-variable->vector (operands expr))))

(define (lift-variable->vector-apply expr)
  (cons (lift-variable->vector (operator expr))
	(map (lambda (a) (list 'vector (lift-variable->vector a))) (operands expr))))

(define (lift-variable->vector-set! expr)
  (list 'vector-set! (set!-var expr) 0 (lift-variable->vector (set!-val expr))))

;; desugar syntax sugar
;; and -> if
;; or -> if
(define (desugar expr)
  (cond ((immediate? expr) expr)
	((variable? expr) expr)
	((quote? expr) expr)
	((set!? expr) (list 'set! (set!-var expr) (desugar (set!-val expr))))
	((if? expr) (list 'if
			  (desugar (if-test expr))
			  (desugar (if-conseq expr))
			  (desugar (if-altern expr))))
	((begin? expr)
	 (cons 'begin (map desugar (begin-seq expr))))
	((lambda? expr)
	 (cons 'lambda
	       (cons (lambda-args expr)
		     (desugar-body (lambda-body expr)))))
	((let? expr) (desugar-let expr))
	((let*? expr) (desugar-let* expr))
	((letrec? expr) (desugar-letrec expr))
	((letrec*? expr) (desugar-letrec expr))
	((and? expr) (desugar-and expr))
	((or? expr) (desugar-or expr))
	((cond? expr) (desugar-cond expr))
	(else ;; apply
	 (desugar-application expr))))

(define (desugar-let* expr)
  (let* ((b* (let-bindings expr))
	 (lhss (map lhs b*))
	 (rhss (map (lambda (b) (desugar (rhs b))) b*)))
    (cons 'let*
	  (cons (map (lambda (l r) (list l r)) lhss rhss)
		(map desugar (let-body expr))))))

(define (desugar-application expr)
  (map desugar expr))

(define (desugar-and expr)
  (define (f es)
    (if (null? (cdr es))
	(car es)
	(list 'if (desugar (car es)) (f (cdr es)) #f)))
  (if (null? (and-exps expr))
      #t
      (f (and-exps expr))))

(define (desugar-or expr)
  (define (f es)
    (if (null? (cdr es))
	(desugar (car es))
	(list 'if (desugar (car es)) (desugar (car es)) (f (cdr es)))))
  (if (null? (or-exps expr))
      #t
      (f (or-exps expr))))

(define (desugar-let expr)
  (let* ((bs (let-bindings expr))
	 (vars (map lhs bs))
	 (vals (map (lambda (b) (desugar (rhs b))) bs))
	 (body (desugar-body (let-body expr))))
    (cons (cons 'lambda (cons vars body)) vals)))

(define (desugar-letrec expr)
  (let* ((bs (let-bindings expr))
	 (vars (map lhs bs))
	 (vals (map (lambda (b) (desugar (rhs b))) bs))
	 (body (desugar-body (let-body expr))))
    (desugar-let (cons 'let
		       (cons (map list vars (make-uniform-n-list #f (length vars)))
			     (append (map (lambda (v e) (list 'set! v e)) vars vals)
				     body))))))

(define (desugar-cond expr)
  (desugar
   (let loop ((cs (cond-clauses expr)))
     (if (null? cs)
	 #f
	 (let ((c (car cs)) (rest (cdr cs)))
	   (if (eq? 'else (cond-test c))
	       (cons 'begin (cond-exps c))
	       (if (null? (cond-exps c))
		   (list 'if
			 (cond-test c)
			 (cond-test c)
			 (loop rest))
		   (list 'if
			 (cond-test c)
			 (cons 'begin
			       (cond-exps c))
			 (loop rest)))))))))


(define (desugar-body seq)
  (let loop ((rest seq) (defines '()))
    (let ((e (car rest)))
      (if (define? e)
	  (loop (cdr rest) (cons e defines))
	  (treat-internal-define rest defines)))))

(define (normalize-define def)
  (if (define-proc? def)
      (list 'define
	    (define-proc-name def)
	    (cons 'lambda
		  (cons (define-proc-params def)
			(define-proc-body def))))
      def))

(define (treat-internal-define es defines)
  (if (null? defines)
      (map desugar es)
      (let* ((defines (map normalize-define defines))
	     (def-names (map define-name defines))
	     (def-exps (map define-exp defines)))
	(list (desugar-let (append (list 'let
					 (map (lambda (n) (list n #f)) def-names))
				   (append (map (lambda (n e) (list 'set! n e)) def-names def-exps)
					   (map desugar es))))))))
