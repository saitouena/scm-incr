(load "./utils.scm")

(define (header)
  (format #t "section .text\n")
  (format #t "\tglobal scheme_entry\n")
  (format #t "scheme_entry:\n"))

(define (ret)
  (format #t "\tret\n"))

;; cf. https://practical-scheme.net/gauche/man/gauche-refj/Shu-Zhi-.html#g_t_6570_5024_306e_6f14_7b97
;; cf. runtime.c (runtime representation)
;; TODO: use binary literal. see gauche reference manual, or see nanopass.
(define int-shift 2)
(define int-mask #b11)
(define int-tag #b00)
(define (int-rep x)
  (logior (ash x int-shift) int-tag))

(define char-shift 8)
(define char-mask #b11111111)
(define char-tag 15)
(define (char-rep x)
  (logior (ash (char->integer x) char-shift) char-tag))

(define boolean-mask #b1111111)
(define boolean-tag  #b0011111)
(define c-true-rep 159)
(define c-false-rep 31) ;; 31+2^7=159
(define (bool-rep x)
  (if x c-true-rep c-false-rep))

(define c-nil-rep 47)

(define pair-mask #b111)
(define pair-tag #b001)

(define vector-mask #b111)
(define vector-tag #b010)

;; for '() and ()
(define (mynull? obj)
  (equal? (list 'quote '()) obj))

(define (immediate? x)
  (or (integer? x) (char? x) (boolean? x) (mynull? x)))

;; scheme -> runtime representation
(define (immediate-rep x)
  (cond
   ((integer? x) (int-rep x))
   ((char? x) (char-rep x))
   ((boolean? x) (bool-rep x))
   ((mynull? x) c-nil-rep)
   (else
    (error "unsupported type."))))

(define (compile-integer x)
  (format #t "\tmov rax, ~s\n" x))

(define (operator expr)
  (car expr))

(define (primcall-operand1 expr) (cadr expr))

(define (unary-primcall? expr)
  (and (= (length expr) 2)
       (symbol? (operator expr))))

(define (compile-unary-primcall expr env si)
  (compile-expr (primcall-operand1 expr) env si)
  (let ((op (operator expr)))
    (cond ((eq? op 'add1)
	   (compile-add1))
	  ((eq? op 'sub1)
	   (compile-sub1))
	  ((eq? op 'integer->char)
	   (compile-integer->char))
	  ((eq? op 'char->integer)
	   (compile-char->integer))
	  ((eq? op 'zero?)
	   (compile-zero?))
	  ((eq? op 'null?)
	   (compile-null?))
	  ((eq? op 'not)
	   (compile-not))
	  ((eq? op 'integer?)
	   (compile-integer?))
	  ((eq? op 'boolean?)
	   (compile-boolean?))
	  ((eq? op 'char?)
	   (compile-char?))
	  ((eq? op 'car)
	   (compile-car))
	  ((eq? op 'cdr)
	   (compile-cdr))
	  ((eq? op 'pair?)
	   (compile-pair?))
	  ((eq? op 'make-vector)
	   (compile-make-vector))
	  ((eq? op 'vector?)
	   (compile-vector?))
	  ((eq? op 'vector-length)
	   (compile-vector-length))
	  (else
	   (error "unknown primcall")))))

(define (compile-add1)
  (printf "\tadd rax, ~s\n" (immediate-rep 1)))

(define (compile-sub1)
  (printf "\tsub rax, ~s\n" (immediate-rep 1)))

(define (compile-integer->char)
  (printf "\tshl rax, ~s\n" (- char-shift int-shift)) ;; << 6
  (printf "\tor rax, ~s\n" char-tag)) ;; mask it

(define (compile-char->integer)
  (printf "\tshr rax, ~s\n" (- char-shift int-shift))) ;; >> 6

(define (compile-zero?) (compile-predicate 0))
(define (compile-null?) (compile-predicate ''()))
(define (compile-not) (compile-predicate #f)) ;; return #t iff arg0 is #f
(define (compile-integer?)
  (compile-type-p int-mask int-tag))

(define (compile-char?)
  (compile-type-p char-mask char-tag))

(define (compile-boolean?)
  (compile-type-p boolean-mask boolean-tag))

(define (compile-predicate v)
  (emit-label (unique-label))
  (printf "\tcmp rax, ~s\n" (immediate-rep v)) ;; cmpl?
  (printf "\tjne .L0\n")
  (printf "\tmov rax, ~s\n" (immediate-rep #t))
  (printf "\tjmp .L1\n")
  (printf ".L0:\n")
  (printf "\tmov rax, ~s\n" (immediate-rep #f))
  (printf ".L1:\n"))

(define (compile-type-p mask tag)
  (emit-label (unique-label))
  (printf "\tand rax, ~s\n" mask)
  (printf "\tcmp rax, ~s\n" tag) ;; cmpl?
  (printf "\tjne .L0\n")
  (printf "\tmov rax, ~s\n" (immediate-rep #t))
  (printf "\tjmp .L1\n")
  (printf ".L0:\n")
  (printf "\tmov rax, ~s\n" (immediate-rep #f))
  (printf ".L1:\n"))

(define car-offset -1)
(define cdr-offset 7)

(define (compile-car)
  (emit "mov rax, [rax+(~a)]" car-offset))

(define (compile-cdr)
  (emit "mov rax, [rax+(~a)]" cdr-offset))

(define (compile-pair?) (compile-type-p pair-mask pair-tag))

;; TODO: understand why this doesn't work
;; (define (compile-make-vector)
;;   (emit "mov [rdi], rax") ;; op dst, src
;;   (emit "mov rdx, rdi")
;;   (emit "or rdx, ~s" #b010) ;; save return value to rdx
;;   (emit "mov rbx, rdi")
;;   (emit "shr rax, ~s" 2) ;; >> 2
;;   (emit "inc rax") ;; for the storage of length
;;   (bump-rax)
;;   (emit "mov rdi, rax")
;;   (emit "mov rax, rdx"))

(define (compile-make-vector)
  (emit "mov [rdi], rax") ;; op dst, src
  (emit "mov rbx, rdi")
  (emit "shr rax, ~s" 2) ;; >> 2
  (emit "inc rax") ;; for the storage of length
  (bump-rax)
  (emit "mov rdi, rax")
  (emit "mov rax, rbx")
  (emit "or rax, ~s" #b010))

(define (compile-vector-length)
  (emit "and rax, ~s" -8) ;; #b1...11000
  (emit "mov rax, [rax]"))

(define (compile-vector?)
  (compile-type-p vector-mask vector-tag))

(define (binary-primcall? expr)
  (and (= (length expr) 3)
       (symbol? (operator expr))))

(define binary-operand1 cadr)
(define binary-operand2 caddr)

(define (compile-binary-primcall expr env si)
  (compile-expr (binary-operand2 expr) env si)
  (printf "\tmov [rsp+(~a)], rax\n" si)
  (compile-expr (binary-operand1 expr) env (- si wordsize))
  (let ((op (operator expr)))
    (cond ((eq? op '+) (compile-plus si))
	  ((eq? op '-) (compile-minus si))
	  ((eq? op '*) (compile-mult si))
	  ((eq? op '<) (compile-lt si))
	  ((eq? op '=) (compile-num-eq si))
	  ((eq? op 'char=?) (compile-char-eq si))
	  ((eq? op 'cons) (compile-cons si))
	  ((eq? op 'vector-ref) (compile-vector-ref si))
	  (else
	   (error "unsupported binary primcall.")))))

(define (compile-plus si)
  (printf "\tadd rax, [rsp+(~a)]\n" si))

(define (compile-minus si)
  (printf "\tsub rax, [rsp+(~a)]\n" si))

(define (compile-mult si)
  (printf "\tmul qword [rsp+(~a)]\n" si)
  (printf "\tshr rax, 2\n")) ;; internal representation has ..00 tag, so we need >>2 operation.

(define (compile-cons si)
  ;; rax -> 1st operand, [rsp+si] -> 2nd operand
  (emit "mov rbx, [rsp+(~a)]" si)
  (emit "mov [rdi+(~a)], rbx" wordsize)
  (emit "mov [rdi], rax")
  (emit "mov rax, rdi")
  (emit "or rax, ~a" #b001)
  (emit "add rdi, ~a" (* wordsize 2)))

(define (compile-compare inst si)
  (emit-label (unique-label))
  (printf "\tcmp rax, [rsp+(~a)]\n" si)
  (printf "\t~s .L0\n" inst)
  (printf "\tmov rax, ~a\n" (immediate-rep #t))
  (printf "\tjmp .L1\n")
  (printf ".L0:\n")
  (printf "\tmov rax, ~a\n" (immediate-rep #f))
  (printf ".L1:\n"))

(define (compile-lt si) ;; I'm not sure.
  (compile-compare 'jg si))

(define (compile-num-eq si)
  (compile-compare 'jne si))

(define (compile-char-eq si)
  (compile-compare 'jne si))

;; https://practical-scheme.net/gauche/man/gauche-refj/bekuta.html
;; vector-ref vector k :optional fallback
(define (compile-vector-ref si)
  ;; rax -> 1st operand, [rsp+si] -> 2nd operand
  (emit "and rax, ~a" -8)
  (emit "mov rbx, rax")
  (emit "mov rax, [rsp+(~s)]" (- si wordsize))
  (emit "shr rax, ~s" 2)
  (emit "inc rax")
  (bump-rax)
  (emit "mov rax, [rax]"))

;; ! rcx is not reserved !
;; rax = rbx+wordsize*rax
(define (bump-rax)
  (emit "mov rcx, ~s" wordsize)
  (emit "mul rcx")
  (emit "add rax, rbx"))

(define (arg3-primcall? expr)
  (and (= (length expr) 4)
       (symbol? (operator expr))))

(define operand1 cadr)
(define operand2 caddr)
(define operand3 cadddr)
(define (compile-arg3-primcall expr env si)
;;  (display expr)
;;  (newline)
  (compile-expr (operand3 expr) env si)
  (emit "mov [rsp+(~a)], rax" si)
  (compile-expr (operand2 expr) env (- si wordsize))
  (emit "mov [rsp+(~a)], rax" (- si wordsize))
  (compile-expr (operand1 expr) env (- si wordsize wordsize))
  ;; [rsp+si] -> 3rd operand
  ;; [rsp+si-wordsize] -> 2nd operand
  ;; rax -> 1st operand
  (let ((op (operator expr)))
    (cond ((eq? op 'vector-set!)
	   (compile-vector-set! si))
	  (else
	   (error "unsupproted 3arg primcall. expr=" expr)))))

;; https://practical-scheme.net/gauche/man/gauche-refj/bekuta.html
;; vector-set! vector k obj
(define (compile-vector-set! si)
  ;; rax -> v
  ;; [rsp+si-wordsize] -> k
  ;; [rsp+si] -> obj
  (emit "and rax, ~a" -8) ;; -8 = 0b111....1000
  (emit "mov rbx, rax")
  (emit "mov rax, [rsp+(~s)]" (- si wordsize))
  (emit "shr rax, ~s" 2) ;; >> 2
  (emit "inc rax")
  (bump-rax)
  (emit "mov rbx, [rsp+(~s)]" si)
  (emit "mov [rax], rbx")
  (emit "mov rax, ~s" (immediate-rep #t))) ;; return #t

(define variable? symbol?)

(define (compile-variable var env si)
  (emit "mov rax, [rsp+(~a)]" (lookup-environment var env)))

(define (lookup-environment var env)
  (if (null? env)
      (error "undefined variable -- LOOKUP ENVIRONMENT env=" env)
      (let* ((b (car env))
	     (bvar (bind-var b))
	     (bval (bind-rhs b)))
	(if (eq? var bvar)
	    bval
	    (lookup-environment var (cdr env))))))

(define (let*? expr)
  (tagged-list? expr 'let*))

(define (bind-var bind)
  (car bind))

(define (bind-rhs bind)
  (cdr bind))

(define (let-bind-rhs bind)
  (cadr bind))

(define (extend-env var si env)
  (cons (cons var si) env))

(define (compile-let* bs body env si)
  (if (null? bs)
      (compile-sequence body env si)
      (let* ((b (car bs))
	     (var (bind-var b))
	     (expr (let-bind-rhs b)))
	(compile-expr expr env si)
	(emit "mov [rsp+(~a)], rax" si)
	(compile-let* (cdr bs) body (extend-env var si env) (- si wordsize)))))

;; (let* ((x 3) (y x)) expr)
(define let-bindings cadr)
(define let-body cddr)

(define (compile-if test conseq altern env si)
  (let ((L0 (unique-label)) (L1 (unique-label)))
    (compile-expr test env si)
    (emit "cmp rax, ~a" (immediate-rep #f))
    (emit "je ~s" L0)
    (compile-expr conseq env si)
    (emit "jmp ~s" L1)
    (emit-label L0)
    (compile-expr altern env si)
    (emit-label L1)))

(define (if? expr)
  (tagged-list? expr 'if))

(define if-test cadr)
(define if-conseq caddr)
(define if-altern cadddr)

(define (begin? expr)
  (tagged-list? expr 'begin))

(define (begin-seq expr)
  (cdr expr))

(define (compile-sequence seq env si)
;;  (display seq)
;;  (newline)
  (if (null? (cdr seq))
      (compile-expr (car seq) env si)
      (begin
	(compile-expr (car seq) env si)
	(compile-sequence (cdr seq) env si))))

(define vararg-primcalls
  (list 'vector))

(define (vararg-primcall? expr)
  (memq (car expr) vararg-primcalls))

(define operands cdr)

(define (compile-vararg-primcall expr env si)
  (let ((op (operator expr)))
    (cond ((eq? op 'vector)
	   (compile-expr (vector->let (operands expr)) env si))
	  (else
	   (error "unsupported vararg primcall expr=" expr)))))

(define (vector->let vs)
  (define (unroll vs idx)
    (if (null? vs)
	'(*vec*)
	(cons `(vector-set! *vec* ,idx ,(car vs))
	      (unroll (cdr vs) (+ idx 1)))))
  (let* ((sz (length vs)))
    (cons 'let*
	  (cons
	   `((*vec* (make-vector ,sz)))
	   (unroll vs 0)))))

(define (compile-expr expr env si)
;;  (display expr)
;;  (newline)
  (cond ((immediate? expr)
	 (printf "\tmov rax, ~s\n" (immediate-rep expr)))
	((variable? expr)
	 (compile-variable expr env si))
	((let*? expr)
	 (compile-let* (let-bindings expr) (let-body expr) env si))
	((if? expr)
	 (compile-if (if-test expr) (if-conseq expr) (if-altern expr) env si))
	((begin? expr)
	 (compile-sequence (begin-seq expr) env si))
	((vararg-primcall? expr)
	 (compile-vararg-primcall expr env si))
	((unary-primcall? expr)
	 (compile-unary-primcall expr env si))
	((binary-primcall? expr)
	 (compile-binary-primcall expr env si))
	((arg3-primcall? expr)
	 (compile-arg3-primcall expr env si))
	(else
	 (error "unsupported expression. expr=" expr))))

(define wordsize 8) ;; 8 byte
(define init-sp (- wordsize))
(define init-env '())

(define (compile-main expr)
  (header)
  (compile-expr expr init-env init-sp)
  (ret))

(define (build-program expr)
  (with-output-to-file "output.asm"
    (lambda () (compile-main expr)))
  (sys-system "nasm -f elf64 output.asm")
  (sys-system "gcc -g -c runtime.c")
  (sys-system "gcc output.o runtime.o"))

(define (output-program expr) ;; for debug
  (compile-main expr))
