(define (header)
  (format #t "section .text\n")
  (format #t "\tglobal scheme_entry\n")
  (format #t "scheme_entry:\n"))

(define (ret)
  (format #t "\tret\n"))

(define (compile-integer x)
  (format #t "\tmov eax, ~s\n" x))

(define (compile-and-emit)
  (let ((prog (read)))
    (header)
    (compile-integer prog)
    (ret)))

(with-output-to-file "output.asm"
  (lambda ()
    (with-input-from-file "source.scm" compile-and-emit)))
