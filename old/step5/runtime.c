#include <stdio.h>

int scheme_entry();

const int fixnum_mask = 0b11; // 2 bits
const int fixnum_tag = 0b00;//=0
const int fixnum_shift = 2;
const int char_mask = 0b11111111; // 8 bits
const int char_tag = 0b00001111; // =15
const int char_shift = 8;
const int boolean_mask = 0b1111111; // 7 bits
const int boolean_tag =  0b0011111;//=31
const int boolean_shift = 7;
const int nil = 0b00101111;//=47

void print_char(char ch) {
  switch (ch) {
  case '\t':
    printf("#\\tab\n");
    break;
  case '\n':
    printf("#\\newline\n");
    break;
  case ' ':
    printf("#\\space\n");
    break;
  default:
    printf("#\\%c\n",ch);
  }
}

int main() {
  int val = scheme_entry();
  if((val&fixnum_mask)==fixnum_tag)
    printf("%d\n", val>>fixnum_shift);
  else if(val==nil) {
    printf("()\n");
  } else if((val&char_mask)==char_tag) {
    char ch = (char)(val>>char_shift);
    print_char(ch);
  } else if((val&boolean_mask)==boolean_tag) {
    int b=val>>boolean_shift;
    if(b==0) {
      printf("#f\n");
    } else {
      printf("#t\n");
    }
  } else {
    printf("unknown type.\n");
  }
  return 0;
}
