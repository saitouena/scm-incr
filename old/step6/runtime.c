#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>

int64_t scheme_entry(void* heap);

const int fixnum_mask = 0b11; // 2 bits
const int fixnum_tag = 0b00;//=0
const int fixnum_shift = 2;
const int char_mask = 0b11111111; // 8 bits
const int char_tag = 0b00001111; // =15
const int char_shift = 8;
const int boolean_mask = 0b1111111; // 7 bits
const int boolean_tag =  0b0011111;//=31
const int boolean_shift = 7;
const int nil = 0b00101111;//=47

void print_char(char ch) {
  switch (ch) {
  case '\t':
    printf("#\\tab");
    break;
  case '\n':
    printf("#\\newline");
    break;
  case ' ':
    printf("#\\space");
    break;
  default:
    printf("#\\%c",ch);
  }
}

void print_obj(int64_t val) {
  if((val&fixnum_mask)==fixnum_tag)
    printf("%" PRId64, val>>fixnum_shift);
  else if(val==nil) {
    printf("()");
  } else if((val&char_mask)==char_tag) {
    char ch = (char)(val>>char_shift);
    print_char(ch);
  } else if((val&boolean_mask)==boolean_tag) {
    int64_t b=val>>boolean_shift;
    if(b==0) {
      printf("#f");
    } else {
      printf("#t");
    }
  } else {
    printf("unknown type.");
  }
  
}

void newline() { putchar('\n'); }

int main() {
  void* heap = malloc(sizeof(int64_t)*100);
  int64_t val = scheme_entry(heap);
  print_obj(val);
  newline();
  return 0;
}
