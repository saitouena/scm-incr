(define printf
  (lambda x
    (apply format #t x))) ;; from nanopass

(define (emit . x)
  (printf "\t")
  (apply printf x)
  (printf "\n"))

(define (emit-label label) ;; label is symbol
  (printf "~s:\n" label))

(define (tagged-list? obj tag)
  (and (list? obj)
       (> (length obj) 0)
       (eq? (car obj) tag)))

(define *label-number* 0)
(define (unique-label)
  (set! *label-number* (+ *label-number* 1))
  (string->symbol (string-append "L" (number->string (- *label-number* 1)))))

(define (unique-label-f)
  (set! *label-number* (+ *label-number* 1))
  (string->symbol (string-append "fXX" (number->string (- *label-number* 1)))))

(define lhs car)
(define rhs cadr)

(define (make-unique-label label)
  (set! *label-number* (+ *label-number* 1))
  (string->symbol (string-append (symbol->string label) (number->string (- *label-number* 1)))))

;; set library
(define empty-set '())

(define (make-set . ls) ls)

(define (list->set ls) ls)

(define (set->list s) s)

(define (insert s e)
  (if (memq e s)
      s
      (cons e s)))

(define (contain? s e)
  (memq e s))

(define (union s1 s2)
  (if (null? s1)
      s2
      (let ((e (car s1))
	    (rest (cdr s1)))
	(if (memq e s2)
	    (union rest s2)
	    (cons e (union rest s2))))))

(define (intersect s1 s2)
  (if (null? s1)
      '()
      (if (memq (car s1) s2)
	  (cons (car s1) (intersect (cdr s1) s2))
	  (intersect (cdr s1) s2))))

(define (diff s1 s2) ;; s1 - s2
  (if (null? s1)
      '()
      (if (memq (car s1) s2)
	  (diff (cdr s1) s2)
	  (cons (car s1) (diff (cdr s1) s2)))))

(define (equal-as-set? s1 s2)
  (and (eq? (diff s1 s2) '()) (eq? (diff s2 s1) '())))


(define *unique-var-number* 0)

(define (make-n-unique-vars n)
  (if (= n 0)
      '()
      (cons (make-unique-var) (make-n-unique-vars (- n 1)))))

(define (make-unique-var)
  (set! *unique-var-number* (+ *unique-var-number* 1))
  (string->symbol (string-append "tempXX" (number->string (- *unique-var-number* 1)))))

