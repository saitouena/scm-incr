(define *library*
  '(
    ;; lift primitive to closure
    ;; unary
    (define add1 (lambda (x) (%add1 x)))
    (define sub1 (lambda (x) (%sub1 x)))
    (define integer->char (lambda (x) (%integer->char x)))
    (define char->integer (lambda (x) (%char->integer x)))
    (define zero? (lambda (x) (%zero? x)))
    (define null? (lambda (x) (%null? x)))
    (define not (lambda (x) (%not x)))
    (define integer? (lambda (x) (%integer? x)))
    (define boolean? (lambda (x) (%boolean? x)))
    (define char? (lambda (x) (%char? x)))
    (define car (lambda (x) (%car x)))
    (define cdr (lambda (x) (%cdr x)))
    (define pair? (lambda (x) (%pair? x)))
    (define make-vector (lambda (x) (%make-vector x)))
    (define vector? (lambda (x) (%vector? x)))
    (define vector-length (lambda (x) (%vector-length x)))
    ;; binary
    (define + (lambda (a b) (%+ a b)))
    (define - (lambda (a b) (%- a b)))
    (define * (lambda (a b) (%* a b)))
    (define < (lambda (a b) (%< a b)))
    (define = (lambda (a b) (%= a b)))
    (define char=? (lambda (a b) (%char=? a b)))
    (define cons (lambda (a b) (%cons a b)))
    (define vector-ref (lambda (a b) (%vector-ref a b)))
    (define eq? (lambda (a b) (%eq? a b)))
    ;; 3 args
    (define vector-set! (lambda (a b c) (%vector-set! a b c)))
    ;; TODO vararg
    ;; vector
    ;; (define vector (lambda ( . a) (%vector a)))

    ;; lib
    (define (length ls)
      (if (null? ls)
	  0
	  (add1 (length (cdr ls)))))
    (define (map1 f ls)
      (if (null? ls)
	  '()
	  (cons (f (car ls)) (map1 f (cdr ls)))))
    ))
