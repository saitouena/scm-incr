#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>

int64_t scheme_entry(void* heap);

const int64_t fixnum_mask = 0b11; // 2 bits
const int64_t fixnum_tag = 0b00;//=0
const int64_t fixnum_shift = 2;
const int64_t char_mask = 0b11111111; // 8 bits
const int64_t char_tag = 0b00001111; // =15
const int64_t char_shift = 8;
const int64_t boolean_mask = 0b1111111; // 7 bits
const int64_t boolean_tag =  0b0011111;//=31
const int64_t boolean_shift = 7;
const int64_t nil = 0b00101111;//=47
const int64_t pair_mask = 0b111;
const int64_t pair_tag = 0b001;
const int64_t vector_mask = 0b111;
const int64_t vector_tag = 0b010;
const int64_t closure_mask = 0b111;
const int64_t closure_tag = 0b110;

void print_obj(int64_t val);

void print_char(char ch) {
  switch (ch) {
  case '\t':
    printf("#\\tab");
    break;
  case '\n':
    printf("#\\newline");
    break;
  case ' ':
    printf("#\\space");
    break;
  default:
    printf("#\\%c",ch);
  }
}

void print_list(int64_t val) {
  int64_t* car = (int64_t*)(val-1);
  int64_t* cdr = (int64_t*)(val+7);
  print_obj(*car);
  if((*cdr&pair_mask) == pair_tag) {
    printf(" ");
    print_list(*cdr);
  } else if(*cdr==nil) {
    printf(")");
  } else {
    printf(" . ");
    print_obj(*cdr);
    putchar(')');
  } 
}

void print_obj(int64_t val) {
  if((val&fixnum_mask)==fixnum_tag)
    printf("%" PRId64, val>>fixnum_shift);
  else if(val==nil) {
    printf("()");
  } else if((val&char_mask)==char_tag) {
    char ch = (char)(val>>char_shift);
    print_char(ch);
  } else if((val&boolean_mask)==boolean_tag) {
    int64_t b=val>>boolean_shift;
    if(b==0) {
      printf("#f");
    } else {
      printf("#t");
    }
  } else if((val&pair_mask)==pair_tag) {
    printf("(");
    print_list(val);
  } else if((val&vector_mask)==vector_tag) {
    int64_t* vec = (int64_t*)(val-2);
    int64_t sz = (*vec)>>fixnum_shift;
    vec++;
    printf("#(");
    for(int i=0; i<sz; i++) {
      if(i==0)
	print_obj(*vec);
      else {
	printf(" ");
	print_obj(*vec);
      }
      vec++;
    }
    putchar(')');
  } else if((val&closure_mask)==closure_tag) {
    printf("<closure>");
  } else {
    printf("unknown type.");
  }
  
}

void newline() { putchar('\n'); }

int main() {
  void* heap = malloc(sizeof(int64_t)*1000000);
  int64_t val = scheme_entry(heap);
  print_obj(val);
  newline();
  free(heap);
  return 0;
}
