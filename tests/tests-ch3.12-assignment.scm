(add-tests-with-string-output "find bug"
			      [(let* ((f (lambda () (vector 1 2 3)))
				      (v (f)))
				 (vector-set! v 0 10)
				 (vector-ref v 0)) => "10\n"]
			      ;; [(let* ((f (vector (lambda (c)
			      ;; 			   (cons (lambda (v) (vector-set! c 0 (vector-ref v 0)))
			      ;; 				 (lambda () (vector-ref c 0)))))))
			      ;; 	 (let* ((p (vector ((vector-ref f 0) (vector 0)))))
			      ;; 	   ((car (vector-ref p 0)) (vector 12))
			      ;; 	   ((cdr (vector-ref p 0))))) => "12\n"] ;; TODO: pass this case
			      )

(add-tests-with-string-output "set!"
			      [(let* ((x 1))
				 (set! x 10)
				 x) => "10\n"]
			      [(let* ((f (lambda (x) (lambda (c) (if (= c 0) x (set! x -1)))))
				      (a (f 1))
				      (b (f 2)))
				 (a 1)
				 (b 0)) => "2\n"]
			      [(let* ((f (lambda (x) (lambda (c) (if (= c 0) x (set! x -1)))))
				      (a (f 1))
				      (b (f 2)))
				 (a 1)
				 (b 1)
				 (b 0)) => "-1\n"]
			      [(let* ((a 1))
				 (set! a (+ a 1))
				 a) => "2\n"]
			      )

