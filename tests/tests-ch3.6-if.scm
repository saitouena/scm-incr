(add-tests-with-string-output "if form"
			      [(if #t 1 2) => "1\n"]
			      [(if (not #t) 3 4) => "4\n"]
			      )
