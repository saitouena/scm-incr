(add-tests-with-string-output "length"
			      [(length '(1 2 3 4)) => "4\n"]
			      )

(add-tests-with-string-output "map1"
			      [(map1 add1 '(1 2 3 4)) => "(2 3 4 5)\n"]
			      )
