(add-tests-with-string-output "let* form"
			      [(let* ((x 3)) x) => "3\n"]
			      [(let* ((x 20)) (+ x x)) => "40\n"]
			      [(let* ((x 3) (y (+ x 3))) y) => "6\n"]
			      [(let* ((x 3) (y (let* ((z 30)) (+ x z)))) y) => "33\n"]
			      [(let* ((x 3)) (let* ((y (+ x 10))) (let* ((z y)) z))) => "13\n"]
			      [(let* () 1 2 3) => "3\n"]
			      )
