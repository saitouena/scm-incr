(add-tests-with-string-output "tail call optimization"
			      [((lambda () 1 2 3)) => "3\n"]
			      [(let* ((x 1) (y 2) (g (lambda (x) (+ x y))))
				 (begin
				   (g 10)
				   (g 3))
				 (begin
				   1
				   (g y))) => "4\n"]
			      )
