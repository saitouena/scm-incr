(use gauche.test)

(test-start "utils test")

(load "./utils.scm")

(test-section "equality of set")

(test* "1"
       #t
       (equal-as-set? (make-set 1 2 3) (make-set 3 2 1)))

(test* "2"
       #f
       (equal-as-set? (make-set 1) (make-set 1 2 3)))

(test* "3"
       #t
       (equal-as-set? (make-set) (make-set)))

(test-end)
