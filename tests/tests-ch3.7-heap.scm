(add-tests-with-string-output "begin"
			      [(begin 1 2) => "2\n"]
			      [(begin (+ 2 3) (+ 2 4)) => "6\n"]
			      )

(add-tests-with-string-output "cons cell"
			      [(cons 1 2) => "(1 . 2)\n"]
			      [(let* ((x 3) (y (+ x 1))) (cons x y))  => "(3 . 4)\n"]
			      [(cons 1 (cons 2 3)) => "(1 2 . 3)\n"]
			      [(cons (+ 1 1) (+ 2 2)) => "(2 . 4)\n"]
			      [(car (cons 1 2)) => "1\n"]
			      [(cdr (cons 3 4)) => "4\n"]
			      [(car (cdr (cons 1 (cons 2 3)))) => "2\n"]
			      [(pair? (cons 1 2)) => "#t\n"]
			      [(pair? 1) => "#f\n"]
			      [(pair? #\a) => "#f\n"]
			      )

(add-tests-with-string-output "vector"
			      [(vector-length (make-vector 10)) => "10\n"]
			      [(vector? (make-vector 10)) => "#t\n"]
			      [(vector? 1) => "#f\n"]
			      [(vector? #\a) => "#f\n"]
			      [(vector? #t) => "#f\n"]
;;			      [(let* ((v (make-vector 1))) (vector-set! v 0 v) v) => "#(#(#(...))\n"] ;; segv case (circular reference)
			      [(let* ((v (make-vector 5))) (vector-set! v 3 10) (vector-ref v 3)) => "10\n"]
			      [(let* ((v (make-vector 3))) (vector-set! v 0 0) (vector-set! v 1 1) (vector-set! v 2 2) v) => "#(0 1 2)\n"]
			      [(vector 1 2 3 4 5) => "#(1 2 3 4 5)\n"]
			      [(cons 1 (cons (vector 1 2 3) 3)) => "(1 #(1 2 3) . 3)\n"]
			      [(let* ((v (vector 1 2 3)))
				 (vector-set! v 2 100)
				 (vector-ref v 2)) => "100\n"]
			      )

(add-tests-with-string-output "allocation pointer"
			      [(let* ((p (cons 3 4)) (v (make-vector 2))) p) => "(3 . 4)\n"]
			      )
