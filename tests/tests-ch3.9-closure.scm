(add-tests-with-string-output "closure"
			      ;; [(labels ((f (codes (x) () x)))
			      ;; 	       (let* ((x 1)
			      ;; 		      (y 2))
			      ;; 		 (closure f x y))) => "<closure>\n"]
			      ;; [(labels ((f (codes (x) (y) (+ x y))))
			      ;; 	       (let* ((y 2))
			      ;; 		 (closure f y))) => "<closure>\n"]
			      ;; [(labels ((f (codes (x) (y) (+ x y))))
			      ;; 	       (let* ((y 2)
			      ;; 		      (fn (closure f y)))
			      ;; 		 (funcall fn 1))) => "3\n"]
			      ;; [(labels ((f (codes (x) (y) (+ x y)))
			      ;; 		(g (codes () (y) (closure f y))))
			      ;; 	       (let* ((y 10)
			      ;; 		      (fn (closure g y)))
			      ;; 		 (funcall (funcall fn) 1))) => "11\n"]
			      [(lambda (x) (+ x x)) => "<closure>\n"]
			      [(let* ((x 5)) (lambda (y) (lambda () (+ x y))))
			       => "<closure>\n"]
			      [((lambda (n) ;; factorial from SICP 4.21
				  ((lambda (fact)
				     (fact fact n))
				   (lambda (ft k)
				     (if (= k 1)
					 1
					 (* k (ft ft (- k 1)))))))
				5) => "120\n"]
			      [((lambda (n) ;; fibonacci from SICP 4.21 a
				  ((lambda (fib)
				     (fib fib n))
				   (lambda (fb k)
				     (if (= k 0)
					 0
					 (if (= k 1)
					     1
					     (+ (fb fb (- k 1)) (fb fb (- k 2))))))))
				5) => "5\n"]
			      [((lambda (x) ;; even? form SICP 4.21 b
				  ((lambda (even? odd?)
				     (even? even? odd? x))
				   (lambda (ev? od? n)
				     (if (= n 0) #t (od? ev? od? (- n 1))))
				   (lambda (ev? od? n)
				     (if (= n 0) #f (ev? ev? od? (- n 1))))))
				10) => "#t\n"]
			      [((lambda (x) ;; even? form SICP 4.21 b
				  ((lambda (even? odd?)
				     (even? even? odd? x))
				   (lambda (ev? od? n)
				     (if (= n 0) #t (od? ev? od? (- n 1))))
				   (lambda (ev? od? n)
				     (if (= n 0) #f (ev? ev? od? (- n 1))))))
				7) => "#f\n"]
			      )
