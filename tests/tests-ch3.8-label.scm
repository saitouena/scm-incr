(add-tests-with-string-output "labels"
			      [(labels ((f (code (x) () (+ x 2)))) (+ 2 3)) => "5\n"]
			      [(labels () (cons 1 2)) => "(1 . 2)\n"]
			      [(labels () (let* ((x 1) (y 2)) (cons x y))) => "(1 . 2)\n"]
			      [(labels ((f (code (x) () (+ x 2)))) (labelcall f 1)) => "3\n"]
			      [(labels ((f (code (x y) () (+ x y)))) (labelcall f 1 2))=> "3\n"]
			      [(labels
				((f (code (x) () (labelcall g x)))
				 (g (code (x) () (+ x 1))))
				(labelcall f 1)) => "2\n"]
			      [(labels
				((f (code (x) () (+ x 10)))
				 (g (code (x) () (+ x 20))))
				(+ (labelcall f 1) (labelcall g 2))) => "33\n"]
			      [(labels
				((fact (code (n) () (if (= n 0) 1 (* (labelcall fact (- n 1)) n)))))
				(labelcall fact 5)) => "120\n"]
			      )
