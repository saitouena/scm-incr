(use gauche.test)

(test-start "translator test")

(load "./transformer.scm")

(test-section "annotate free variables in lambda expression")
(test* "1"
       '(lambda () () (let* ((x 1) (y x)) (+ x y)))
       (annotate-free-variable '(lambda () (let* ((x 1)
						  (y x))
					     (+ x y)))))

(test* "2" '(+) (free-vars '(let* ((x 1) (y x)) (+ x y))))

(test* "3"
       '(lambda (u) (z) (+ u z (let* ((x 1)) x) (let* ((y 3)) y)))
       (annotate-free-variable
	'(lambda (u) (+ u z (let* ((x 1)) x) (let* ((y 3)) y)))))

(test* "4"
       '(y u + a)
       (free-vars '(let* ((x y) (z u)) (+ x a))))

(test* "5"
       '(x u + a)
       (free-vars '(let* ((x x) (z u)) (+ x a))))

(test* "6"
       '(let ((x 5)) (lambda (y) (x) (lambda () (x y) (+ x y))))
       (annotate-free-variable
	'(let ((x 5)) (lambda (y) (lambda () (+ x y))))))

(test-section "annotate free variables in lambda expression")

(test* "1"
       '(labels () 1)
       (transform 1))

(test* "2"
       '(labels () x)
       (transform 'x))

;; (test* "3"
;;        '((L1
;;        (annotated->labels '(if #t (lambda (x) x) (lambda (y) y))))

(test-section "retrieve quote form")

;; (test* "1"
;;        #t
;;        (equal-as-set? (make-set '(1 . 2))
;; 		      (retrieve-quote-form '(let* ((f (lambda () (quote (1 . 2)))))
;; 					      (eq? (f) (f))))))

(test-end)
