(add-tests-with-string-output "internal define"
			      [(let ()
				 (define x 1)
				 (define y 2)
				 (+ x y)) => "3\n"]
			      [(let ()
				 (define (fact n)
				   (if (= n 0)
				       1
				       (* n (fact (- n 1)))))
				 (fact 5)) => "120\n"]
			      )
