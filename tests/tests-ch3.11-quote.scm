(add-tests-with-string-output "eq?"
  [(eq? (cons 1 2) (cons 1 2)) => "#f\n"]
  [(eq? 1 2) => "#f\n"]
  [(let* ((x (cons 1 2))) (eq? x x)) => "#t\n"]
  [(let* ((v (vector 1))) (eq? v v)) => "#t\n"]
  )

(add-tests-with-string-output "quote form"
			      [(let* ((f (lambda () (quote (1 . 2)))))
				 (eq? (f) (f))) => "#t\n"]
			      [(eq? (quote (1 . 2)) (quote (1 . 2))) => "#t\n"] ;; TODO: this is valid? #f?
)
