(define closure-mask #b111)
(define closure-tag #b110)

;; <Closure> ::= (closure lvar . vars)
;; note: gauche has builtin `closure?` function.
(define (my-closure? expr)
  (tagged-list? expr 'closure))

(define closure-label cadr)
(define closure-vars cddr)

(define (compile-closure-vars base vars var-env free-vars-env)
  (if (null? vars)
      #t ;; nothing to do
      (cond ((lookup-environment (car vars) var-env)
	     (emit "mov rbx, [rsp+(~a)]" (lookup-environment (car vars) var-env))
	     (emit "mov [rdi+(~a)], rbx" base)
	     (compile-closure-vars (+ wordsize base) (cdr vars) var-env free-vars-env))
	    ((lookup-environment (car vars) free-vars-env)
	     (emit "mov rbx, [rsi+(~a)]" (lookup-environment (car vars) free-vars-env))
	     (emit "mov [rdi+(~a)], rbx" base)
	     (compile-closure-vars (+ wordsize base) (cdr vars) var-env free-vars-env))
	    (else
	     (error "variable not found -- COMPILE-CLOSURE-VARS vars,var-env,free-vars-env=" vars var-env free-vars-env)))))

(define (compile-closure label vars var-env label-env free-vars-env)
  (emit "mov rax, ~s" (lookup-environment label label-env))
  (emit "mov [rdi], rax")
  (emit "mov rax, rdi")
  (emit "or rax, ~a" closure-tag)
  (let ((bump-size (* wordsize (+ (length vars) 1))))
    (compile-closure-vars wordsize vars var-env free-vars-env)
    (emit "add rdi, ~a" bump-size)))
